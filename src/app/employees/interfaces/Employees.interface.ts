export interface Employee {
        id: number;
        employeeCode: string;
        employeeFullname: string;
        division: string;
        position: string;
        grade: number;
        beginDate: Date;
        birthday: Date;
        identificationNumber: string;
        totalSalary: number;
        office:string;
    }

    export interface EmployeeRequest {

        Office: string;
        position: string;
        grade: number;
        employeeFilterBy:Number
    }

