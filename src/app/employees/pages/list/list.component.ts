import { Component, OnInit } from '@angular/core';

import { EmployeesService } from '../../services/employees.service';
import { Employee,EmployeeRequest } from '../../interfaces/Employees.interface';

@Component({
  selector: 'app-listado',
  templateUrl: './list.component.html',
  styles: [`
  table {
  width: 100%;
}
button {
  margin: 16px 8px;
}
.mat-row .mat-cell {
  border-bottom: 1px solid transparent;
  border-top: 1px solid transparent;
  cursor: pointer;
}

.mat-row:hover .mat-cell {
  border-color: currentColor;
}
.employee-row-is-clicked {
  font-weight: bold;
  background-color:#7b1fa2 !important;
}
  `
  ]
})
export class ListComponent implements OnInit {

  employees: Employee[] = [];
  employee !: Employee;
  displayedColumns: string[] = ['employeeCode', 'employeeFullname', 'division', 'position',
                                'grade', 'beginDate', 'birthday', 'identificationNumber','totalSalary'];
  
  constructor( private employeesService: EmployeesService ) { }

  ngOnInit(): void {
    this.employeesService.getEmployees()
      .subscribe( employees => this.employees = employees );
  }
  filter(filterType:Number):void {
    if(typeof this.employee === 'undefined'){
      alert("Select One Employee");
    }else if(filterType == 0){
      this.employeesService.getEmployees()
      .subscribe( employees => this.employees = employees );

    }
    else{
      const employeeRequest:EmployeeRequest = {
        Office:this.employee.office,
        grade:this.employee.grade,
        position:this.employee.position,
        employeeFilterBy:filterType
      };
      this.employeesService.filterEmployees(employeeRequest)
      .subscribe( employees => this.employees = employees );
    }

  }
  clickedRow(row:Employee):void{
    console.log(row);
    this.employee = row;
  }

}
