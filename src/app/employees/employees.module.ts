import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '../material/material.module';

import { AddComponent } from './pages/add/add.component';
import { HomeComponent } from './pages/home/home.component';
import { ListComponent } from './pages/list/list.component';
import { EmployeesRoutingModule } from './employees-routing.module';




@NgModule({
  declarations: [
    AddComponent,
    HomeComponent,
    ListComponent,
    
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    EmployeesRoutingModule
  ]
})
export class EmployeesModule { }
