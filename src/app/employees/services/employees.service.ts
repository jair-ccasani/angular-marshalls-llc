import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';
import { Employee,EmployeeRequest } from '../interfaces/Employees.interface';
@Injectable({
  providedIn: 'root'
})
export class EmployeesService {
  
  private baseUrl: string = environment.baseUrl;
  constructor(private http: HttpClient) { }


  getEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(`${ this.baseUrl }/Employee`);
  }

  filterEmployees(request:EmployeeRequest): Observable<Employee[]> {
    
    return this.http.get<Employee[]>(`${ this.baseUrl }/Employee?Office=${request.Office}&Position=${request.position}&Grade=${request.grade}&employeeFilterBy=${request.employeeFilterBy}`);
  }

  getEmployeById( id:  Number ):Observable<Employee> {
    return this.http.get<Employee>(`${ this.baseUrl }/Employee/${ id }`);
  }
}
