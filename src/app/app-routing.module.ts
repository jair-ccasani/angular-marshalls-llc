import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorPageComponent } from './shared/error-page/error-page.component';

const routes : Routes=[
  {
    path:'employees',
    loadChildren:()=>import('./employees/employees.module').then(m=> m.EmployeesModule)
  },
  {
    path:'404',
    component:ErrorPageComponent
  },
  {
    path:'**',
    // component:ErrorPageComponent
    redirectTo:'404'
  }
];

@NgModule({

  imports: [
    RouterModule.forRoot(routes)
  ],
  exports:[
    RouterModule
  ]
})
export class AppRoutingModule { }
